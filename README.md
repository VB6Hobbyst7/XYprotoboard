# XYprotoboard
DIY minimalistic arduino like board for experimenting with small machines. Design is a modified and shrinked version of [Gen7](https://github.com/Traumflug/Generation_7_Electronics) electronics. Modify it with gEDA: 

`sudo apt-get install geda geda-utils geda-xgsch2pcb`.

 New atmega controllers need a bootloader to be flashed via ISP to use it with Arduino IDE. IDE itself also needs a additional board configuration to be setup. For more info check [this](https://github.com/MCUdude/MightyCore) and its references.

### Features
- Cheap to make.
- Single sided (mixed THT and SMT) PCB.
- Two sockets for "stepstick" (Pololu) stepper drivers.
- Two power (PWM) outputs connectors.
- One analog input connector.
- Four digital (switch, encoder) input connectors.
- Unused mcu pins are connected to extensional socket. 
- Onboard USB-UART converter.
- Reset button. 


![BOARD](mainboard-AVR-ETCHING.png)